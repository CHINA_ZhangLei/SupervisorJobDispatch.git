<?php

namespace App\Components\Supervisor\Config;

/**
 * 监听器配置文件
 * Class SupervisorListenerConfig
 *
 * @package App\Components\Supervisor\Config
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 14:35:04
 */
class SupervisorListenerConfig extends SupervisorProgramConfig
{

    /** @var string event事件的类型，也就是说，只有写在这个地方的事件类型。才会被发送  默认：EVENT  监听所有事件 */
    protected $events = 'EVENT';

    /**
     * getEvents
     *
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 14:39:40
     */
    public function getEvents(): string
    {
        return $this->events;
    }

    /**
     * setEvents
     *
     * @param string $events
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 14:39:36
     */
    public function setEvents(string $events): void
    {
        $this->events = $events;
    }

    /**
     * configContent
     *
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 14:39:34
     */
    public function configContent(): string
    {
        $content = parent::configContent();
        $content .= "events={$this->events}\r\n";
        return $content;
    }
}
