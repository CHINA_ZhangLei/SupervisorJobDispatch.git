<?php

namespace App\Components\Supervisor;

/**
 * SuperVisor命令
 * Class SupervisorCommand
 *
 * @package App\Components\Supervisor
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 14:33:37
 */
class SupervisorCommand
{

    public static function supervisorPidPath(): string
    {
        return config('supervisor.pid_file');
    }

    public static function supervisorExecFilePath(): string
    {
        return config('supervisor.exec_file');
    }

    public static function supervisorConfigFilePath(): string
    {
        return config('supervisor.config_file');
    }

    public static function supervisorUsername(): string
    {
        return config('supervisor.username');
    }

    public static function supervisorPassword(): string
    {
        return config('supervisor.password');
    }

    private static function supervisorCtlCommand(): string
    {
        $commands = [trim(self::supervisorExecFilePath()),];
        if (self::supervisorUsername() && self::supervisorPassword()) {
            $commands[] = "-u";
            $commands[] = trim(self::supervisorUsername());
            $commands[] = "-p";
            $commands[] = trim(self::supervisorPassword());
        }
        if (self::supervisorConfigFilePath()) {
            $commands[] = "-c";
            $commands[] = trim(self::supervisorConfigFilePath());
        }
        return implode(" ", $commands);
    }

    public static function shutdownCommand(): string
    {
        return implode(" ", [
            self::supervisorCtlCommand(),
            "shutdown"
        ]);
    }

    public static function reloadCommand(): string
    {
        return implode(" ", [
            self::supervisorCtlCommand(),
            "reload"
        ]);
    }

    public static function rereadCommand(): string
    {
        return implode(" ", [
            self::supervisorCtlCommand(),
            "reread"
        ]);
    }

    public static function updateCommand(): string
    {
        return implode(" ", [
            self::supervisorCtlCommand(),
            "update"
        ]);
    }

    public static function startCommand(string $groupName = "all", array $processNames = []): string
    {
        $process = "all";
        if ($groupName !== $process) {
            $process = empty($processNames) ? implode(" ", $processNames) : "{$groupName}:*";
        }

        return implode(" ", [
            self::supervisorCtlCommand(),
            "start",
            $process
        ]);
    }

    public static function stopCommand(string $groupName = "all", array $processNames = []): string
    {
        $process = "all";
        if ($groupName !== $process) {
            $process = empty($processNames) ? implode(" ", $processNames) : "{$groupName}:*";
        }

        return implode(" ", [
            self::supervisorCtlCommand(),
            "stop",
            $process
        ]);
    }

    public static function restartCommand(string $groupName, array $processNames = []): string
    {
        $process = "all";
        if ($groupName !== $process) {
            $process = empty($processNames) ? implode(" ", $processNames) : "{$groupName}:*";
        }

        return implode(" ", [
            self::supervisorCtlCommand(),
            "restart",
            $process
        ]);
    }

    public static function addCommand(string $name): string
    {
        return implode(" ", [
            self::supervisorCtlCommand(),
            "add",
            $name
        ]);
    }

    public static function removeCommand(string $name): string
    {
        return implode(" ", [
            self::supervisorCtlCommand(),
            "remove",
            $name
        ]);
    }
}
