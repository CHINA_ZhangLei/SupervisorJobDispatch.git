<?php

namespace App\Components\Supervisor;

use App\Components\Supervisor\Config\SupervisorListenerConfig;
use App\Components\Supervisor\Config\SupervisorProgramConfig;
use Exception;
use Illuminate\Support\Facades\Storage;

/**
 * supervisor配置生成
 * Class SupervisorConfig
 *
 * @package App\Components\Supervisor
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 14:40:17
 */
class SupervisorConfig
{

    const SUPERVISOR_PROGRAM_FILENAME      = 'supervisor_program.ini';
    const SUPERVISOR_PROGRAM_TMP_FILENAME  = 'supervisor_program_tmp';
    const SUPERVISOR_LISTENER_FILENAME     = 'supervisor_listener.ini';
    const SUPERVISOR_LISTENER_TMP_FILENAME = 'supervisor_listener_tmp';
    /** @var SupervisorProgramConfig[] */
    public static $processConfigList = [];

    /** @var SupervisorListenerConfig[] */
    public static $listenerConfigList = [];

    /**
     * 生成进程配置文件
     * buildProcessConfigFile
     *
     * @throws Exception
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 14:34:16
     */
    public static function buildProcessConfigFile()
    {
        if (Storage::disk('supervisor')
                   ->exists(static::SUPERVISOR_PROGRAM_TMP_FILENAME) &&
            Storage::disk('supervisor')
                   ->delete(static::SUPERVISOR_PROGRAM_TMP_FILENAME) == false) {
            throw new Exception('supervisord 监控进程临时配置文件删除异常');
        }

        foreach (static::$processConfigList as $processConfig) {
            Storage::disk('supervisor')
                   ->append(static::SUPERVISOR_PROGRAM_TMP_FILENAME, $processConfig->configContent());
        }

        if (Storage::disk('supervisor')
                   ->exists(static::SUPERVISOR_PROGRAM_TMP_FILENAME)) {
            if (Storage::disk('supervisor')
                       ->exists(static::SUPERVISOR_PROGRAM_FILENAME) &&
                Storage::disk('supervisor')
                       ->delete(static::SUPERVISOR_PROGRAM_FILENAME) == false) {
                throw new Exception('supervisord 监控进程配置文件删除异常');
            }

            Storage::disk('supervisor')
                   ->move(static::SUPERVISOR_PROGRAM_TMP_FILENAME, static::SUPERVISOR_PROGRAM_FILENAME);
        }
    }

    /**
     * 生成监听器配置文件
     * buildListenerConfigFile
     *
     * @throws Exception
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 14:33:56
     */
    public static function buildListenerConfigFile()
    {
        if (Storage::disk('supervisor')
                   ->exists(static::SUPERVISOR_LISTENER_TMP_FILENAME) &&
            Storage::disk('supervisor')
                   ->delete(static::SUPERVISOR_LISTENER_TMP_FILENAME) == false) {
            throw new Exception('supervisord 事件监听进程临时配置文件删除异常');
        }

        foreach (static::$listenerConfigList as $listenerConfig) {
            Storage::disk('supervisor')
                   ->append(static::SUPERVISOR_LISTENER_TMP_FILENAME, $listenerConfig->configContent());
        }

        if (Storage::disk('supervisor')
                   ->exists(static::SUPERVISOR_LISTENER_TMP_FILENAME)) {
            if (Storage::disk('supervisor')
                       ->exists(static::SUPERVISOR_LISTENER_FILENAME) &&
                Storage::disk('supervisor')
                       ->delete(static::SUPERVISOR_LISTENER_FILENAME) != true) {
                throw new Exception('supervisord 事件监听进程配置文件删除异常');
            }

            Storage::disk('supervisor')
                   ->move(static::SUPERVISOR_LISTENER_TMP_FILENAME, static::SUPERVISOR_LISTENER_FILENAME);
        }
    }
}
