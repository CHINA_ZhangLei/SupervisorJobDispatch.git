<?php

namespace App\Console\Commands\Supervisor;

use Illuminate\Console\Command;

/**
 * Class SupervisorEventListener
 *
 * @package App\Console\Commands\Supervisor
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 14:51:50
 */
class SupervisorEventListener extends Command
{

    /**
     * @var string
     */
    protected $signature = 'supervisor:event_listener';

    /**
     * @var string
     */
    protected $description = '监听supervisor事件,然后通过接口上报RD平台';


    public function handle(EventListener $eventListener, SupervisorEventManager $supervisorEventManager)
    {
        $eventListener->listen(function (EventListener $listener, EventNotification $event) use ($supervisorEventManager) {
            $supervisorEventManager->report($event);
            return true;
        });
    }
}
