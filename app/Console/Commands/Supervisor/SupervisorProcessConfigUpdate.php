<?php

namespace App\Console\Commands\Supervisor;

use Illuminate\Console\Command;

/**
 * Class SupervisorProcessConfigUpdate
 *
 * @package App\Console\Commands\Supervisor
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 14:54:23
 */
class SupervisorProcessConfigUpdate extends Command
{

    /** @var string  */
    protected $signature = 'supervisor:process_config_update';

    /** @var string  */
    protected $description = '执行频率（5分钟） 根据RD调度平台的进程调度信息，生成supervisor配置文件';

    public function handle()
    {
        $this->info('更新supervisor进程配置');
        (new SupervisorProcessConfigUpdateManager())->buildConfigFile();
    }
}
