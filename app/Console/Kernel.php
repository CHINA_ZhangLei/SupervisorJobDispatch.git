<?php

namespace App\Console;

use App\Components\Supervisor\SupervisorCommand;
use App\Console\Commands\supervisor\SupervisorProcessConfigUpdate;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Laravel\Lumen\Application;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use Symfony\Component\Finder\Finder;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [];


    /**
     * schedule
     *
     * @param Schedule $schedule
     *
     * @author zhanglei8 <zhanglei8@guahao.com>
     * @date   2020-01-07 13:48:07
     */
    protected function schedule(Schedule $schedule)
    {

        //定时生成supervisor配置文件
        $schedule->command(SupervisorProcessConfigUpdate::class)
                 ->everyFiveMinutes()
                 ->runInBackground();

        //每分钟执行一次supervisor配置文件更新
        $schedule->exec(SupervisorCommand::updateCommand())
                 ->everyFiveMinutes()
                 ->runInBackground();

        //每半个小时执行一次supervisor进程状态检测，如果supervisor停机，则执行supervisor重启操作
        $schedule->exec(SupervisorCommand::reloadCommand())
                 ->everyThirtyMinutes()
                 ->when(function () {
                     $supervisorPid = trim(file_get_contents(SupervisorCommand::supervisorPidPath()));
                     return empty($supervisorPid);
                 })
                 ->runInBackground();
    }


    /**
     * getCommands
     *
     * @return array
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 14:42:43
     */
    public function getCommands()
    {
        $commands = $this->load(__DIR__ . '/Commands');
        return array_unique(array_merge($commands, parent::getCommands()));
    }

    /**
     * load
     *
     * @param $paths
     * @return array
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 14:42:56
     */
    protected function load($paths): array
    {
        $paths = array_unique(Arr::wrap($paths));

        $paths = array_filter($paths, function ($path) {
            return is_dir($path);
        });

        if (empty($paths)) {
            return [];
        }

        $namespace = $this->app->getNamespace();

        $commands = [];
        foreach ((new Finder)->in($paths)
                             ->files() as $command) {
            $commands[] = $namespace . str_replace(
                    ['/', '.php'],
                    ['\\', ''],
                    Str::after($command->getPathname(), realpath(app_path()) . DIRECTORY_SEPARATOR)
                );
        }
        return $commands;
    }
}
