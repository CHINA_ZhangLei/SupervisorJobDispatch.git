<?php

namespace App\Contracts\Supervisor\Event;

use Mtdowling\Supervisor\EventNotification;

/**
 * Interface ISupervisorEvent
 *
 * @package App\Contracts\SuperviosrEvent
 *
 * @author zhanglei8 <zhanglei8@guahao.com>
 * @date 2020-02-19 22:09:11
 */
interface ISupervisorEvent
{

    /**
     * report
     *
     * @param EventNotification $event
     * @return void
     *
     * @author zhanglei8 <zhanglei8@guahao.com>
     * @date 2020-02-19 22:17:48
     */
    public function report(EventNotification $event): void;
}
