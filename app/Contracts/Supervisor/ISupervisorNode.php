<?php

namespace App\Contracts\Supervisor;

use App\Infrastructure\Bean\Supervisor\SupervisorNodeBean;

/**
 * interface ISupervisorNode
 *
 * @package App\Contracts\SupervisorEvent\Node
 *
 * @author zhanglei8 <zhanglei8@guahao.com>
 * @date 2020-02-25 15:00:35
 */
interface ISupervisorNode
{
    /**
     * get
     *
     * @param string $hostName
     * @return SupervisorNodeBean|null
     *
     * @author zhanglei8 <zhanglei8@guahao.com>
     * @date 2020-02-25 15:03:43
     */
    public function get(string $hostName);

    /**
     * save
     *
     * @param string $hostName
     * @param string $hostIP
     * @param int $processPid
     * @param int $status
     * @param
     * @return mixed
     *
     * @author zhanglei8 <zhanglei8@guahao.com>
     * @date 2020-02-25 15:08:42
     */
    public function save(string $hostName,string $hostIP,int $processPid,int $status = 1):bool ;

    /**
     * 上报supervisor节点状态
     * report
     *
     * @param int $status
     * @return bool
     *
     * @author zhanglei8 <zhanglei8@guahao.com>
     * @date 2020-02-25 15:41:37
     */
    public function report(int $status = 1):bool ;
}
