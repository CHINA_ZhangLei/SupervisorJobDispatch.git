<?php

namespace App\Contracts\Supervisor;

use App\Infrastructure\Bean\Supervisor\SupervisorProcessBean;

/**
 * interface ISupervisorProcess
 *
 * @package App\Contracts\Supervisor
 *
 * @author zhanglei8 <zhanglei8@guahao.com>
 * @date 2020-02-25 19:12:11
 */
interface ISupervisorProcess
{

    /**
     * 修改进程状态
     * changeState
     *
     * @param string $processName 进程名称
     * @param int $state 进程状态
     * @param string $lifeCycleDesc 进程状态流程详情
     * @return mixed
     *
     * @author zhanglei8 <zhanglei8@guahao.com>
     * @date 2020-02-25 19:13:36
     */
    public function changeState(string $processName, int $state, string $lifeCycleDesc = '');

    /**
     * 获取当前supervisor节点下需要运行的进程列表
     * processes
     *
     * @param array $scheduleGlobalIdSet
     * @param string $supervisorGlobalId
     * @return SupervisorProcessBean[]
     *
     * @author zhanglei8 <zhanglei8@guahao.com>
     * @date 2020-02-25 19:20:36
     */
    public function processes(array $scheduleGlobalIdSet, string $supervisorGlobalId): array;
}
