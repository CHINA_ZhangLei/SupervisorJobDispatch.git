<?php

namespace App\Contracts\Supervisor;

use App\Infrastructure\Bean\Supervisor\SupervisorScheduleBean;

/**
 * Interface ISupervisorSchedule
 *
 * @package App\Contracts\Supervisor
 *
 * @author zhanglei8 <zhanglei8@guahao.com>
 * @date 2020-02-25 19:52:03
 */
interface ISupervisorSchedule
{

    /**
     * schedules
     *
     * @return SupervisorScheduleBean[]
     *
     * @author zhanglei8 <zhanglei8@guahao.com>
     * @date 2020-02-25 19:53:56
     */
    public function schedules():array;
}
