<?php

namespace App\Enums;

use Rexlabs\Enum\Enum;

/**
 * 性别类型
 * Class SexEnum
 *
 * @package App\Enums
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 14:29:22
 */
class SexEnum extends Enum
{
    const MAN     = 1;
    const WOMAN   = 2;
    const UNKNOWN = 3;


    /**
     * map
     *
     * @return array
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 14:29:40
     */
    public static function map(): array
    {
        return [
            static::MAN     => '男',
            static::WOMAN   => '女',
            static::UNKNOWN => '保密',
        ];
    }
}