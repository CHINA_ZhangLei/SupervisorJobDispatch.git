<?php

namespace App\Enums\Supervisor;

use Rexlabs\Enum\Enum;

/**
 * supervisor进程状态
 * Class SupervisorProcessStateEnum
 *
 * @package App\Enums\Supervisor
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 14:29:50
 */
class SupervisorProcessStateEnum extends Enum
{
    /**
     * 1: stopped     进程由stopping转换到stopped
     * 2: starting    进程由stopped转换到starting
     * 3: running     进程由starting转换到running
     * 4: stopping    进程由running转换到stopping
     * 5: exited      进程由running转换到exited
     * 6: backoff     进程由starting转换到backoff，可以指定重启次数
     * 7: fatal       进程由backoff转换到fatal,超过指定重启次数并放弃重试
     * 8: unkonwn     由于supervisor程序问题造成的进程位置错误
     */

    const SUPERVISOR_PROCESS_STATE_STOPPED  = 'PROCESS_STATE_STOPPED';
    const SUPERVISOR_PROCESS_STATE_STARTING = 'PROCESS_STATE_STARTING';
    const SUPERVISOR_PROCESS_STATE_RUNNING  = 'PROCESS_STATE_RUNNING';
    const SUPERVISOR_PROCESS_STATE_STOPPING = 'PROCESS_STATE_STOPPING';
    const SUPERVISOR_PROCESS_STATE_EXITED   = 'PROCESS_STATE_EXITED';
    const SUPERVISOR_PROCESS_STATE_BACKOFF  = 'PROCESS_STATE_BACKOFF';
    const SUPERVISOR_PROCESS_STATE_FATAL    = 'PROCESS_STATE_FATAL';
    const SUPERVISOR_PROCESS_STATE_UNKNOWN  = 'PROCESS_STATE_UNKNOWN';


    /**
     * map
     *
     * @return array
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 14:30:12
     */
    public static function map(): array
    {
        return [
            static::SUPERVISOR_PROCESS_STATE_STOPPED  => 1,
            static::SUPERVISOR_PROCESS_STATE_STARTING => 2,
            static::SUPERVISOR_PROCESS_STATE_RUNNING  => 3,
            static::SUPERVISOR_PROCESS_STATE_STOPPING => 4,
            static::SUPERVISOR_PROCESS_STATE_EXITED   => 5,
            static::SUPERVISOR_PROCESS_STATE_BACKOFF  => 6,
            static::SUPERVISOR_PROCESS_STATE_FATAL    => 7,
            static::SUPERVISOR_PROCESS_STATE_UNKNOWN  => 8,
        ];
    }
}
