<?php

namespace App\Events\SupervisorEvent;

use App\Events\Event;
use Mtdowling\Supervisor\EventNotification;

/**
 * 所有的supervisor事件都可以触发次时间
 * Class SupervisorEvent
 *
 * @package App\Events\SupervisorEvent
 *
 * @author zhanglei8 <zhanglei8@guahao.com>
 * @date 2020-02-19 23:38:24
 */
class SupervisorEvent extends Event
{


    /** @var EventNotification */
    protected $eventNotification;

    public function __construct(EventNotification $eventNotification)
    {
        $this->eventNotification = $eventNotification;
    }

    /**
     * @return EventNotification
     *
     * @author zhanglei8 <zhanglei8@guahao.com>
     */
    public function getEventNotification(): EventNotification
    {
        return $this->eventNotification;
    }

}
