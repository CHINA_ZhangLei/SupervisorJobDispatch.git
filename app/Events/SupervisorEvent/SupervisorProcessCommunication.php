<?php

namespace App\Events\SupervisorEvent;

/**
 * Class SupervisorProcessCommunication
 *
 * @package App\Events\SupervisorEvent
 *
 * @author zhanglei8 <zhanglei8@guahao.com>
 * @date 2020-02-19 23:43:05
 */
class SupervisorProcessCommunication extends SupervisorEvent
{

}
