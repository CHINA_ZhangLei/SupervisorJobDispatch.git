<?php

namespace App\Events\SupervisorEvent;

/**
 * Class SupervisorProcessLog
 *
 * @package App\Events\SupervisorEvent
 *
 * @author zhanglei8 <zhanglei8@guahao.com>
 * @date 2020-02-19 23:41:49
 */
class SupervisorProcessLog extends SupervisorEvent
{

}
