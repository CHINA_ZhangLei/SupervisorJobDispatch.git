<?php

namespace App\Events\SupervisorEvent;

use App\Events\Event;
use Mtdowling\Supervisor\EventNotification;

/**
 * Class SupervisorProcessState
 *
 * @package App\Events\SupervisorEvent
 *
 * @author zhanglei8 <zhanglei8@guahao.com>
 * @date 2020-02-19 23:33:26
 */
class SupervisorProcessState extends SupervisorEvent
{

}
