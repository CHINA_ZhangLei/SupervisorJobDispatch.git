<?php

namespace App\Events\SupervisorEvent;

/**
 * Class SupervisorRemoteCommunication
 *
 * @package App\Events\SupervisorEvent
 *
 * @author zhanglei8 <zhanglei8@guahao.com>
 * @date 2020-02-19 23:41:24
 */
class SupervisorRemoteCommunication extends SupervisorEvent
{

}
