<?php

namespace App\Events\SupervisorEvent;

/**
 * Class SupervisorStateChange
 *
 * @package App\Events\SupervisorEvent
 *
 * @author zhanglei8 <zhanglei8@guahao.com>
 * @date 2020-02-19 23:43:31
 */
class SupervisorStateChange extends SupervisorEvent
{

}
