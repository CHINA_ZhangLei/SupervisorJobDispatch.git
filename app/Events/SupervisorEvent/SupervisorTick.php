<?php

namespace App\Events\SupervisorEvent;

/**
 * Class SuperVisorTick
 *
 * @package App\Events\SupervisorEvent
 *
 * @author zhanglei8 <zhanglei8@guahao.com>
 * @date 2020-02-19 23:43:58
 */
class SupervisorTick extends SupervisorEvent
{

}
