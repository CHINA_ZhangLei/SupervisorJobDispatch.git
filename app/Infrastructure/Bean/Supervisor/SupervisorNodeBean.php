<?php

namespace App\Infrastructure\Bean\Supervisor;

/**
 * Class SupervisorNodeBean
 *
 * @package App\Infrastructure\Bean\Supervisor
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 15:04:11
 */
class SupervisorNodeBean
{

    /** @var int supervisord节点表主键ID */
    private $id;
    /** @var int supervisord节点表业务ID */
    private $globalId;
    /** @var string supervisord节点所在主机名称 */
    private $hostName;
    /** @var string supervisord节点所在主机IP地址 */
    private $hostIP;
    /** @var int supervisord节点进程号 */
    private $processPid;
    /** @var  int supervisord节点运行状态 1:running  2：stoped */
    private $status;
    /** @var int supervisord节点挂起控制 软控制 1：挂起  2：取消挂起 */
    private $hangUp;
    /** @var string 节点创建时间 */
    private $gmtCreated;
    /** @var string  节点更新时间 */
    private $gmtModified;

    /**
     * getId
     *
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:04:19
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * getGlobalId
     *
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:04:23
     */
    public function getGlobalId(): int
    {
        return $this->globalId;
    }

    /**
     * @param int $globalId
     */
    public function setGlobalId(int $globalId): void
    {
        $this->globalId = $globalId;
    }

    /**
     * getHostName
     *
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:04:27
     */
    public function getHostName(): string
    {
        return $this->hostName;
    }

    /**
     * @param string $hostName
     */
    public function setHostName(string $hostName): void
    {
        $this->hostName = $hostName;
    }

    /**
     * getHostIP
     *
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:04:31
     */
    public function getHostIP(): string
    {
        return $this->hostIP;
    }

    /**
     * @param string $hostIP
     */
    public function setHostIP(string $hostIP): void
    {
        $this->hostIP = $hostIP;
    }

    /**
     * getProcessPid
     *
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:04:33
     */
    public function getProcessPid(): int
    {
        return $this->processPid;
    }

    /**
     * @param int $processPid
     */
    public function setProcessPid(int $processPid): void
    {
        $this->processPid = $processPid;
    }

    /**
     * getStatus
     *
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:04:36
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * getHangUp
     *
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:04:39
     */
    public function getHangUp(): int
    {
        return $this->hangUp;
    }

    /**
     * @param int $hangUp
     */
    public function setHangUp(int $hangUp): void
    {
        $this->hangUp = $hangUp;
    }

    /**
     * getGmtCreated
     *
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:04:42
     */
    public function getGmtCreated(): string
    {
        return $this->gmtCreated;
    }

    /**
     * @param string $gmtCreated
     */
    public function setGmtCreated(string $gmtCreated): void
    {
        $this->gmtCreated = $gmtCreated;
    }

    /**
     * getGmtModified
     *
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:04:45
     */
    public function getGmtModified(): string
    {
        return $this->gmtModified;
    }

    /**
     * @param string $gmtModified
     */
    public function setGmtModified(string $gmtModified): void
    {
        $this->gmtModified = $gmtModified;
    }

}
