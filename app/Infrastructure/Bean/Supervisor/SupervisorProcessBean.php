<?php

namespace App\Infrastructure\Bean\Supervisor;

/**
 * Class SupervisorProcessBean
 *
 * @package App\Infrastructure\Bean\Supervisor
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 15:04:52
 */
class SupervisorProcessBean
{
    /** @var int 主键ID */
    private $id;
    /** @var string 调度任务业务ID */
    private $scheduleGlobalId;
    /** @var string supervisor节点业务ID */
    private $supervisorGlobalId;
    /** @var string 进程名称 */
    private $name;
    /** @var string 进程启动参数 */
    private $shell;
    /** @var int 进程状态  1：进程可用  2：进程禁用 */
    private $status;
    /**
     * @var int 进程运行状态
     * 1: stopped     进程未启动|由stopping转换到stopped
     * 2: starting    进程由stopped转换到starting
     * 3: running     进程由starting转换到running
     * 4: stopping    进程由running转换到stopping
     * 5: exited      进程由running转换到exited
     * 6: backoff     进程由starting转换到backoff，可以指定重启次数
     * 7: fatal       进程由backoff转换到fatal,超过指定重启次数并放弃重试
     * 8: unkonwn     由于supervisor程序问题造成的进程位置错误
     */
    private $runStatus;
    /** @var string 进程生命周期流转详细信息 */
    private $lifeCycleDesc;
    /** @var string 进程创建时间 */
    private $gmtCreated;
    /** @var string 进程更新时间 */
    private $gmtModified;

    /**
     * getId
     *
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:05:04
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * setId
     *
     * @param int $id
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:06:49
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * getScheduleGlobalId
     *
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:05:07
     */
    public function getScheduleGlobalId(): string
    {
        return $this->scheduleGlobalId;
    }

    /**
     * setScheduleGlobalId
     *
     * @param string $scheduleGlobalId
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:06:52
     */
    public function setScheduleGlobalId(string $scheduleGlobalId): void
    {
        $this->scheduleGlobalId = $scheduleGlobalId;
    }

    /**
     * getSupervisorGlobalId
     *
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:05:10
     */
    public function getSupervisorGlobalId(): string
    {
        return $this->supervisorGlobalId;
    }

    /**
     * setSupervisorGlobalId
     *
     * @param string $supervisorGlobalId
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:05:12
     */
    public function setSupervisorGlobalId(string $supervisorGlobalId): void
    {
        $this->supervisorGlobalId = $supervisorGlobalId;
    }

    /**
     * getName
     *
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:05:16
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * setName
     *
     * @param string $name
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:05:18
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * getShell
     *
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:05:21
     */
    public function getShell(): string
    {
        return $this->shell;
    }

    /**
     * setShell
     *
     * @param string $shell
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:05:56
     */
    public function setShell(string $shell): void
    {
        $this->shell = $shell;
    }

    /**
     * getStatus
     *
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:05:59
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * setStatus
     *
     * @param int $status
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:06:01
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * getRunStatus
     *
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:06:04
     */
    public function getRunStatus(): int
    {
        return $this->runStatus;
    }

    /**
     * setRunStatus
     *
     * @param int $runStatus
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:06:27
     */
    public function setRunStatus(int $runStatus): void
    {
        $this->runStatus = $runStatus;
    }

    /**
     * getLifeCycleDesc
     *
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:06:29
     */
    public function getLifeCycleDesc(): string
    {
        return $this->lifeCycleDesc;
    }

    /**
     * setLifeCycleDesc
     *
     * @param string $lifeCycleDesc
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:06:31
     */
    public function setLifeCycleDesc(string $lifeCycleDesc): void
    {
        $this->lifeCycleDesc = $lifeCycleDesc;
    }

    /**
     * getGmtCreated
     *
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:06:34
     */
    public function getGmtCreated(): string
    {
        return $this->gmtCreated;
    }

    /**
     * setGmtCreated
     *
     * @param string $gmtCreated
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:06:38
     */
    public function setGmtCreated(string $gmtCreated): void
    {
        $this->gmtCreated = $gmtCreated;
    }

    /**
     * getGmtModified
     *
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:06:40
     */
    public function getGmtModified(): string
    {
        return $this->gmtModified;
    }

    /**
     * setGmtModified
     *
     * @param string $gmtModified
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:06:43
     */
    public function setGmtModified(string $gmtModified): void
    {
        $this->gmtModified = $gmtModified;
    }

}
