<?php

namespace App\Infrastructure\Bean\Supervisor;

use App\Infrastructure\Factory\supervisor\SupervisorProcessFactory;

/**
 * Class SupervisorScheduleBean
 *
 * @package App\Infrastructure\Bean\Supervisor
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 15:07:28
 */
class SupervisorScheduleBean
{
    /** @var int */
    private $id;
    /** @var string 调度任务业务ID */
    private $globalId;
    /** @var string 调度任务标题 */
    private $title;
    /** @var string  调度任务名称 作为进程名称的一部分 */
    private $name;
    /** @var string  开发语言的全路径执行文件 */
    private $language;
    /** @var string 程序执行目录 */
    private $directory;
    /** @var string cli 脚本执行参数 例如 artisan:schedule:run */
    private $shell;
    /** @var int  脚本类型 1：一次脚本  2：定时脚本  3：常驻脚本 */
    private $type;
    /** @var int  脚本状态 1：可以执行 2：不可执行 */
    private $status;
    /** @var int 脚本启动时间  时间戳 */
    private $timeBegin;
    /** @var int 脚本截止时间  时间戳 */
    private $timeEnd;
    /** @var string 定时任务规则  和crontab规则相同 */
    private $cron;
    /** @var int 脚本是否随supervisord启动而启动 1:是 2：否 */
    private $autoStart;
    /** @var int 脚本启动失败重试次数  默认：3 */
    private $startRetries;
    /** @var int 进程退出自动重启  1：总是自动重启 2：永不自动重启 3：正常退出的情况自动重启 */
    private $autoRestart;
    /** @var int 调度任务总共启动进程数量 */
    private $processNum;
    /** @var string 调度任务描述 */
    private $introduction;
    /** @var int 进程启动优先级  0-999 */
    private $priority;
    /** @var string 调度任务创建日期 */
    private $gmtCreated;
    /** @var string 调度任务更新日期 */
    private $gmtModified;
    /** @var SupervisorProcessBean[] */
    private $supervisorProcesses;

    /**
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getGlobalId(): string
    {
        return $this->globalId;
    }

    /**
     * @param string $globalId
     */
    public function setGlobalId(string $globalId): void
    {
        $this->globalId = $globalId;
    }

    /**
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage(string $language): void
    {
        $this->language = $language;
    }

    /**
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getDirectory(): string
    {
        return $this->directory;
    }

    /**
     * @param string $directory
     */
    public function setDirectory(string $directory): void
    {
        $this->directory = $directory;
    }

    /**
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getShell(): string
    {
        return $this->shell;
    }

    /**
     * @param string $shell
     */
    public function setShell(string $shell): void
    {
        $this->shell = $shell;
    }

    /**
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getTimeBegin(): int
    {
        return $this->timeBegin;
    }

    /**
     * @param int $timeBegin
     */
    public function setTimeBegin(int $timeBegin): void
    {
        $this->timeBegin = $timeBegin;
    }

    /**
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getTimeEnd(): int
    {
        return $this->timeEnd;
    }

    /**
     * @param int $timeEnd
     */
    public function setTimeEnd(int $timeEnd): void
    {
        $this->timeEnd = $timeEnd;
    }

    /**
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getCron(): string
    {
        return $this->cron;
    }

    /**
     * @param string $cron
     */
    public function setCron(string $cron): void
    {
        $this->cron = $cron;
    }

    /**
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getAutoStart(): int
    {
        return $this->autoStart;
    }

    /**
     * @param int $autoStart
     */
    public function setAutoStart(int $autoStart): void
    {
        $this->autoStart = $autoStart;
    }

    /**
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getStartRetries(): int
    {
        return $this->startRetries;
    }

    /**
     * @param int $startRetries
     */
    public function setStartRetries(int $startRetries): void
    {
        $this->startRetries = $startRetries;
    }

    /**
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getAutoRestart(): int
    {
        return $this->autoRestart;
    }

    /**
     * @param int $autoRestart
     */
    public function setAutoRestart(int $autoRestart): void
    {
        $this->autoRestart = $autoRestart;
    }

    /**
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getProcessNum(): int
    {
        return $this->processNum;
    }

    /**
     * @param int $processNum
     */
    public function setProcessNum(int $processNum): void
    {
        $this->processNum = $processNum;
    }

    /**
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getIntroduction(): string
    {
        return $this->introduction;
    }

    /**
     * @param string $introduction
     */
    public function setIntroduction(string $introduction): void
    {
        $this->introduction = $introduction;
    }

    /**
     * @return int
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getGmtCreated(): string
    {
        return $this->gmtCreated;
    }

    /**
     * @param string $gmtCreated
     */
    public function setGmtCreated(string $gmtCreated): void
    {
        $this->gmtCreated = $gmtCreated;
    }

    /**
     * @return string
     *
     * @author zhanglei <1070968166@qq.com>
     */
    public function getGmtModified(): string
    {
        return $this->gmtModified;
    }

    /**
     * @param string $gmtModified
     */
    public function setGmtModified(string $gmtModified): void
    {
        $this->gmtModified = $gmtModified;
    }

    /**
     * getSupervisorProcess
     *
     * @param string $supervisorGlobalId
     * @return SupervisorProcessBean []
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-02-25 20:09:45
     */
    public function getSupervisorProcess(string $supervisorGlobalId): array
    {
        if (empty($this->supervisorProcesses)) {
            $this->supervisorProcesses = SupervisorProcessFactory::produce()
                                                                 ->processes(
                                                                     [$this->getGlobalId()],
                                                                     $supervisorGlobalId
                                                                 );
        }
        return $this->supervisorProcesses;
    }
}
