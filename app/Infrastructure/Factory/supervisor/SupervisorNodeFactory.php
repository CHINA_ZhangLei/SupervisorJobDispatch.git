<?php

namespace App\Infrastructure\Factory\supervisor;

use App\Contracts\Supervisor\ISupervisorNode;
use App\Infrastructure\Bean\Supervisor\SupervisorNodeBean;
use App\Infrastructure\Model\Supervisor\SupervisorNodeModel;
use App\Infrastructure\Repostory\Supervisor\SupervisorNode;

/**
 * Class SupervisorNodeFactory
 *
 * @package App\Infrastructure\Factory\supervisor
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 15:08:11
 */
class SupervisorNodeFactory
{

    /** @var ISupervisorNode */
    private static $instance;

    /**
     * produce
     *
     * @return ISupervisorNode
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:08:47
     */
    public static function produce(): ISupervisorNode
    {
        if (is_null(self::$instance) || !(self::$instance instanceof ISupervisorNode)) {
            self::$instance = new SupervisorNode();
        }
        return self::$instance;
    }

    /**
     * 生成supervisorNodeBean
     * produceSupervisorNode
     *
     * @param SupervisorNodeModel $supervisorNodeModel
     * @return SupervisorNodeBean
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:08:32
     */
    public static function produceSupervisorNode(SupervisorNodeModel $supervisorNodeModel): SupervisorNodeBean
    {
        $supervisorNodeBean = new SupervisorNodeBean();
        $supervisorNodeBean->setId($supervisorNodeModel->id);
        $supervisorNodeBean->setGlobalId($supervisorNodeModel->global_id);
        $supervisorNodeBean->setHostName($supervisorNodeModel->host_name);
        $supervisorNodeBean->setHostIP($supervisorNodeModel->host_ip);
        $supervisorNodeBean->setStatus($supervisorNodeModel->status);
        $supervisorNodeBean->setHangUp($supervisorNodeModel->hang_up);
        $supervisorNodeBean->setGmtCreated($supervisorNodeModel->gmt_created);
        $supervisorNodeBean->setGmtModified($supervisorNodeModel->gmt_modified);
        return $supervisorNodeBean;
    }

}
