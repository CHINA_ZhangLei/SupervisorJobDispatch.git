<?php

namespace App\Infrastructure\Factory\supervisor;

use App\Contracts\Supervisor\ISupervisorProcess;
use App\Infrastructure\Bean\Supervisor\SupervisorProcessBean;
use App\Infrastructure\Model\Supervisor\SupervisorProcessModel;
use App\Infrastructure\Repostory\Supervisor\SupervisorProcess;

/**
 * Class SupervisorProcessFactory
 *
 * @package App\Infrastructure\Factory\supervisor
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 15:08:57
 */
class SupervisorProcessFactory
{

    /** @var ISupervisorProcess */
    private static $instance;

    /**
     * produce
     *
     * @return ISupervisorProcess
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:09:01
     */
    public static function produce(): ISupervisorProcess
    {
        if (is_null(self::$instance) || !(self::$instance instanceof ISupervisorProcess)) {
            self::$instance = new SupervisorProcess();
        }
        return self::$instance;
    }

    /**
     * 生成supervisorProcessBean
     * produceSupervisorProcess
     *
     * @param SupervisorProcessModel $supervisorProcess
     * @return SupervisorProcessBean
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:09:10
     */
    public static function produceSupervisorProcess(SupervisorProcessModel $supervisorProcess): SupervisorProcessBean
    {
        $supervisorProcessBean = new SupervisorProcessBean();
        $supervisorProcessBean->setId($supervisorProcess->id);
        $supervisorProcessBean->setName($supervisorProcess->name);
        $supervisorProcessBean->setShell($supervisorProcess->shell);
        $supervisorProcessBean->setScheduleGlobalId($supervisorProcess->schedule_global_id);
        $supervisorProcessBean->setSupervisorGlobalId($supervisorProcess->supervisor_global_id);
        $supervisorProcessBean->setStatus($supervisorProcess->status);
        $supervisorProcessBean->setRunStatus($supervisorProcess->run_status);
        $supervisorProcessBean->setGmtCreated($supervisorProcess->gmt_created);
        $supervisorProcessBean->setGmtModified($supervisorProcess->gmt_modified);
        return $supervisorProcessBean;
    }

}
