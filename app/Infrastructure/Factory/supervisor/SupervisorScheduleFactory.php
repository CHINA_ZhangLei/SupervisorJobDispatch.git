<?php

namespace App\Infrastructure\Factory\supervisor;

use App\Contracts\Supervisor\ISupervisorSchedule;
use App\Infrastructure\Bean\Supervisor\SupervisorScheduleBean;
use App\Infrastructure\Model\Supervisor\SupervisorScheduleModel;
use App\Infrastructure\Repostory\Supervisor\SupervisorSchedule;

/**
 * Class SupervisorScheduleFactory
 *
 * @package App\Infrastructure\Factory\supervisor
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 15:09:17
 */
class SupervisorScheduleFactory
{

    /** @var ISupervisorSchedule */
    private static $instance;

    /**
     * produce
     *
     * @return ISupervisorSchedule
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:09:20
     */
    public static function produce(): ISupervisorSchedule
    {
        if (is_null(self::$instance) || !(self::$instance instanceof ISupervisorSchedule)) {
            self::$instance = new SupervisorSchedule();
        }
        return self::$instance;
    }

    /**
     * 生成supervisorProcessBean
     * produceSupervisorSchedule
     *
     * @param SupervisorScheduleModel $supervisorSchedule
     * @return SupervisorScheduleBean
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:09:29
     */
    public static function produceSupervisorSchedule(SupervisorScheduleModel $supervisorSchedule): SupervisorScheduleBean
    {
        $supervisorScheduleBean = new SupervisorScheduleBean();
        $supervisorScheduleBean->setId($supervisorSchedule->id);
        $supervisorScheduleBean->setGlobalId($supervisorSchedule->global_id);
        $supervisorScheduleBean->setTitle($supervisorSchedule->title);
        $supervisorScheduleBean->setName($supervisorSchedule->name);
        $supervisorScheduleBean->setLanguage($supervisorSchedule->language);
        $supervisorScheduleBean->setDirectory($supervisorSchedule->directory);
        $supervisorScheduleBean->setShell($supervisorSchedule->shell);
        $supervisorScheduleBean->setType($supervisorSchedule->type);
        $supervisorScheduleBean->setStatus($supervisorSchedule->status);
        $supervisorScheduleBean->setTimeBegin($supervisorSchedule->time_begin);
        $supervisorScheduleBean->setTimeEnd($supervisorSchedule->time_end);
        $supervisorScheduleBean->setAutoStart($supervisorSchedule->auto_start);
        $supervisorScheduleBean->setAutoRestart($supervisorSchedule->auto_restart);
        $supervisorScheduleBean->setStartRetries($supervisorSchedule->start_retries);
        $supervisorScheduleBean->setProcessNum($supervisorSchedule->num_procs);
        $supervisorScheduleBean->setPriority($supervisorSchedule->priority);
        $supervisorScheduleBean->setIntroduction($supervisorSchedule->introduction);
        $supervisorScheduleBean->setGmtCreated($supervisorSchedule->gmt_created);
        $supervisorScheduleBean->setGmtModified($supervisorSchedule->gmt_modified);
        return $supervisorScheduleBean;
    }

}
