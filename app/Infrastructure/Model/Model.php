<?php

namespace App\Infrastructure\Model;

use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Support\Str;

abstract class Model extends BaseModel
{
    const CREATED_AT = 'gmt_created';

    const UPDATED_AT = 'gmt_modified';

    protected $hidden = [Model::CREATED_AT];

    protected $guarded = ['id', Model::CREATED_AT, Model::UPDATED_AT];

    protected $primaryKey = 'global_id';

    /** @var array 属性类型转换 */
    protected $casts = [
        'gmt_created' => 'string',
        'gmt_modified' => 'string'
    ];

    /**
     * 根据类名获取表名，表名采用无复数的格式，即表名=类名的snake case格式。
     * 比如User类 对应 user 表
     * UserProfile 对应 user_profile 表
     *
     * @return string
     */
    public function getTable()
    {
        if (!isset($this->table)) {
            return str_replace('\\', '', Str::snake(class_basename($this)));
        }
        return $this->table;
    }
}
