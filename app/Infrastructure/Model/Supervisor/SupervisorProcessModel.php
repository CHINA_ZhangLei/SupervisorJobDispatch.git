<?php

namespace App\Infrastructure\Model\Supervisor;

use App\Infrastructure\Model\Model;
use Wedoctor\Convention\Illuminate\Database\Concerns\HasIsDeleted;

/**
 * Class SupervisorProcessModel
 *
 * @package App\Infrastructure\Model\Supervisor
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 15:09:56
 */
class SupervisorProcessModel extends Model
{

    use HasIsDeleted;

    protected $connection = 'RD';

    protected $guarded = [];

    protected $table = 'process';

}
