<?php

namespace App\Infrastructure\Model\Supervisor;

use App\Infrastructure\Model\Model;
use Wedoctor\Convention\Illuminate\Database\Concerns\HasIsDeleted;

/**
 * Class SupervisorScheduleModel
 *
 * @package App\Infrastructure\Model\Supervisor
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 15:10:03
 */
class SupervisorScheduleModel extends Model
{

    use HasIsDeleted;

    protected $connection = 'RD';

    protected $guarded = [];

    protected $table = 'schedule';
}
