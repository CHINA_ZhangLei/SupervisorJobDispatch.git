<?php

namespace App\Infrastructure\Repostory\Supervisor;

use App\Components\Supervisor\SupervisorCommand;
use App\Contracts\Supervisor\ISupervisorNode;
use App\Infrastructure\Factory\supervisor\SupervisorNodeFactory;
use App\Infrastructure\Model\Supervisor\SupervisorNodeModel;
use Bhc\Library\Util\Snowflake;

/**
 * Class SupervisorNode
 *
 * @package App\Infrastructure\Repostory\Supervisor
 *
 * @author zhanglei8 <zhanglei8@guahao.com>
 * @date 2020-02-25 15:12:39
 */
class SupervisorNode implements ISupervisorNode
{

    /**
     * @inheritDoc
     */
    public function get(string $hostName)
    {
        $supervisorNode = SupervisorNodeModel::where('host_name', trim($hostName))->where('status',1)->first();
        return $supervisorNode ? SupervisorNodeFactory::produceSupervisorNode($supervisorNode) : null;
    }

    /**
     * @inheritDoc
     */
    public function save(string $hostName, string $hostIP, int $processPid, int $status = 1): bool
    {
        $record = ['host_name' => $hostName, 'host_ip' => $hostIP, 'process_pid' => $processPid, 'status' => $status,];
        $supervisorNode = $this->get($hostName);
        if ($supervisorNode) {
            $result = SupervisorNodeModel::where('global_id', $supervisorNode->getGlobalId())->update($record);
        } else {
            $record['global_id'] = Snowflake::generateId();
            $record['hang_up'] = 1;
            $result = SupervisorNodeModel::create($record);
        }
        return $result ? true : false;
    }

    /**
     * @inheritDoc
     */
    public function report(int $status = 1): bool
    {
        $hostName = gethostname();
        $hostIp = gethostbyname($hostName);
        $processPid = trim(file_get_contents(SupervisorCommand::supervisorPidPath()));
        return $this->save($hostName, $hostIp, $processPid, $status);
    }
}
