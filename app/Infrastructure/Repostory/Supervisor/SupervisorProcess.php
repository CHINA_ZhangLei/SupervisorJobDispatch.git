<?php

namespace App\Infrastructure\Repostory\Supervisor;

use App\Contracts\Supervisor\ISupervisorProcess;
use App\Infrastructure\Factory\supervisor\SupervisorProcessFactory;
use App\Infrastructure\Model\Supervisor\SupervisorProcessModel;

/**
 * Class SupervisorProcess
 *
 * @package App\Infrastructure\Repostory\Supervisor
 *
 * @author zhanglei8 <zhanglei8@guahao.com>
 * @date 2020-02-25 19:14:04
 */
class SupervisorProcess implements ISupervisorProcess
{

    /**
     * @inheritDoc
     */
    public function changeState(string $processName, int $state, string $lifeCycleDesc = '')
    {
        $supervisorProcess = SupervisorProcessModel::Where('name',$processName)->first();
        if ($supervisorProcess) {
            $supervisorProcess->run_status = $state;
            $supervisorProcess->life_cycle_desc = $lifeCycleDesc;
            $supervisorProcess->save();
        }
    }

    /**
     * @inheritDoc
     */
    public function processes(array $scheduleGlobalIdSet, string $supervisorGlobalId): array
    {
        return SupervisorProcessModel::where('status', 1)
            ->whereIn('schedule_global_id', $scheduleGlobalIdSet)
            ->where('supervisor_global_id', $supervisorGlobalId)
            ->get()
            ->map(function (SupervisorProcessModel $processModel) {
                return SupervisorProcessFactory::produceSupervisorProcess($processModel);
            })->toArray();
    }
}
