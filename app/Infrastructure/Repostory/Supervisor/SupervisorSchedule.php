<?php

namespace App\Infrastructure\Repostory\Supervisor;

use App\Contracts\Supervisor\ISupervisorSchedule;
use App\Infrastructure\Factory\supervisor\SupervisorScheduleFactory;
use App\Infrastructure\Model\Supervisor\SupervisorScheduleModel;

/**
 * Class SupervisorSchedule
 *
 * @package App\Infrastructure\Repostory\Supervisor
 *
 * @author zhanglei8 <zhanglei8@guahao.com>
 * @date 2020-02-25 20:02:39
 */
class SupervisorSchedule implements ISupervisorSchedule
{

    /**
     * @inheritDoc
     */
    public function schedules(): array
    {
        return SupervisorScheduleModel::where('status', 1)
                        ->get()
                        ->map(function (SupervisorScheduleModel $scheduleModel) {
                            return SupervisorScheduleFactory::produceSupervisorSchedule($scheduleModel);
                        })
                        ->toArray();
    }
}
