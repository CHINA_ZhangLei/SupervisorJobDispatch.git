<?php

namespace App\Listeners\SupervisorEventListener;

use App\Events\SupervisorEvent\SupervisorEvent;
use Illuminate\Support\Facades\Log;

/**
 * Class SupervisorEventListener
 *
 * @package App\Listeners\SupervisorEventListener
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 15:02:19
 */
class SupervisorEventListener
{

    /**
     * handle
     *
     * @param SupervisorEvent $supervisorEvent
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:02:22
     */
    public function handle(SupervisorEvent $supervisorEvent)
    {
        Log::info($supervisorEvent->getEventNotification()
                                  ->getEventName() . '    ' . $supervisorEvent->getEventNotification()
                                                                              ->getBody());
    }
}
