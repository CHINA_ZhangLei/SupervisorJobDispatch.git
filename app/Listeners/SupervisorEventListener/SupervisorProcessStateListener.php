<?php

namespace App\Listeners\SupervisorEventListener;

use App\Enums\Supervisor\SupervisorProcessStateEnum;
use App\Events\SupervisorEvent\SupervisorEvent;
use App\Infrastructure\Factory\supervisor\SupervisorProcessFactory;

/**
 * Class SupervisorProcessStateListener
 *
 * @package App\Listeners\SupervisorEventListener
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 15:02:04
 */
class SupervisorProcessStateListener extends SupervisorEventListener
{
    /**
     * handle
     *
     * @param SupervisorEvent $supervisorEvent
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:02:08
     */
    public function handle(SupervisorEvent $supervisorEvent)
    {
        SupervisorProcessFactory::produce()
                                ->changeState(
                                    $supervisorEvent->getEventNotification()
                                                    ->getPool(),
                                    SupervisorProcessStateEnum::valueForKey($supervisorEvent->getEventNotification()
                                                                                            ->getEventName()),
                                    json_encode($supervisorEvent->getEventNotification()
                                                                ->getData())
                                );
    }
}
