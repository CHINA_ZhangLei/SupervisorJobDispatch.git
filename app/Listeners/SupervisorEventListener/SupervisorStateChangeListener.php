<?php

namespace App\Listeners\SupervisorEventListener;

use App\Events\SupervisorEvent\SupervisorEvent;
use App\Infrastructure\Factory\supervisor\SupervisorNodeFactory;
use Mtdowling\Supervisor\EventNotification;

/**
 * Class SupervisorStateChangeListener
 *
 * @package App\Listeners\SupervisorEventListener
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 15:01:55
 */
class SupervisorStateChangeListener extends SupervisorEventListener
{

    /**
     * handle
     *
     * @param SupervisorEvent $supervisorEvent
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:01:59
     */
    public function handle(SupervisorEvent $supervisorEvent)
    {
        switch ($supervisorEvent->getEventNotification()
                                ->getEventName()) {
            case EventNotification::SUPERVISOR_STATE_CHANGE_RUNNING:
                SupervisorNodeFactory::produce()
                                     ->report(1);
                break;
            case EventNotification::SUPERVISOR_STATE_CHANGE_STOPPING:
                SupervisorNodeFactory::produce()
                                     ->report(2);
                break;
        }
    }
}
