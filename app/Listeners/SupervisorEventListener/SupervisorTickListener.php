<?php

namespace App\Listeners\SupervisorEventListener;

use App\Events\SupervisorEvent\SupervisorEvent;
use App\Infrastructure\Factory\supervisor\SupervisorNodeFactory;

/**
 * Class SupervisorTickListener
 *
 * @package App\Listeners\SupervisorEventListener
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 15:01:41
 */
class SupervisorTickListener extends SupervisorEventListener
{

    /**
     * handle
     *
     * @param SupervisorEvent $supervisorEvent
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:01:46
     */
    public function handle(SupervisorEvent $supervisorEvent)
    {
        SupervisorNodeFactory::produce()
                             ->report(1);
    }
}
