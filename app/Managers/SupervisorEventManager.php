<?php

namespace App\Managers;

use App\Contracts\Supervisor\Event\ISupervisorEvent;
use App\Events\SupervisorEvent\SupervisorProcessCommunication;
use App\Events\SupervisorEvent\SupervisorProcessLog;
use App\Events\SupervisorEvent\SupervisorProcessState;
use App\Events\SupervisorEvent\SupervisorRemoteCommunication;
use App\Events\SupervisorEvent\SupervisorStateChange;
use App\Events\SupervisorEvent\SupervisorTick;
use Mtdowling\Supervisor\EventNotification;

/**
 * Class SupervisorEventManager
 *
 * @package App\Managers
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 15:00:20
 */
class SupervisorEventManager implements ISupervisorEvent
{

    /**
     * @inheritDoc
     */
    public function report(EventNotification $event): void
    {
        $supervisorEvent = null;
        switch ($event->getEventName()) {
            case EventNotification::PROCESS_STATE:
            case EventNotification::PROCESS_STATE_STARTING:
            case EventNotification::PROCESS_STATE_RUNNING:
            case EventNotification::PROCESS_STATE_BACKOFF:
            case EventNotification::PROCESS_STATE_STOPPING:
            case EventNotification::PROCESS_STATE_EXITED:
            case EventNotification::PROCESS_STATE_STOPPED:
            case EventNotification::PROCESS_STATE_FATAL:
            case EventNotification::PROCESS_STATE_UNKNOWN:
                $supervisorEvent = new SupervisorProcessState($event);
                break;
            case EventNotification::REMOTE_COMMUNICATION:
                $supervisorEvent = new SupervisorRemoteCommunication($event);
                break;
            case EventNotification::PROCESS_LOG:
            case EventNotification::PROCESS_LOG_STDOUT:
            case EventNotification::PROCESS_LOG_STDERR:
                $supervisorEvent = new SupervisorProcessLog($event);
                break;
            case EventNotification::PROCESS_COMMUNICATION:
            case EventNotification::PROCESS_COMMUNICATION_STDOUT:
            case EventNotification::PROCESS_COMMUNICATION_STDERR:
                $supervisorEvent = new SupervisorProcessCommunication($event);
                break;
            case EventNotification::SUPERVISOR_STATE_CHANGE:
            case EventNotification::SUPERVISOR_STATE_CHANGE_RUNNING:
            case EventNotification::SUPERVISOR_STATE_CHANGE_STOPPING:
                $supervisorEvent = new SupervisorStateChange($event);
                break;
            case EventNotification::TICK:
            case EventNotification::TICK_5:
            case EventNotification::TICK_60:
            case EventNotification::TICK_3600:
                $supervisorEvent = new SupervisorTick($event);
                break;
            default :
                break;
        }

        //调用事件
        $supervisorEvent && event($supervisorEvent);
    }
}
