<?php

namespace App\Managers;

use App\Components\Supervisor\Config\SupervisorProgramConfig;
use App\Components\Supervisor\SupervisorConfig;
use App\Enums\Supervisor\SupervisorProcessStateEnum;
use App\Infrastructure\Bean\Supervisor\SupervisorScheduleBean;
use App\Infrastructure\Factory\supervisor\SupervisorNodeFactory;
use App\Infrastructure\Factory\supervisor\SupervisorScheduleFactory;
use Cron\CronExpression;
use Exception;

/**
 * Class SupervisorProcessConfigUpdateManager
 *
 * @package App\Managers
 *
 * @author  zhanglei <1070968166@qq.com>
 * @date    2020-04-04 15:01:00
 */
class SupervisorProcessConfigUpdateManager
{

    /**
     * buildConfigFile
     *
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:01:05
     */
    public function buildConfigFile()
    {
        try {
            //获取RD是否存在注册的节点信息  存在且未挂起则执行配置获取
            $supervisorNodeBean = SupervisorNodeFactory::produce()
                                                       ->get(gethostname());
            if (is_null($supervisorNodeBean) || $supervisorNodeBean->getHangUp() != 1) {
                return;
            }

            //获取状态为正常的调度任务
            $supervisorSchedules = SupervisorScheduleFactory::produce()
                                                            ->schedules();
            collect($supervisorSchedules)
                ->reject(function (SupervisorScheduleBean $supervisorScheduleBean) use ($supervisorNodeBean) {
                    return $this->supervisorScheduleFilter($supervisorScheduleBean, $supervisorNodeBean->getGlobalId());
                })
                ->each(function (SupervisorScheduleBean $supervisorScheduleBean, $key) use ($supervisorNodeBean) {
                    $this->makeSupervisorProgramConfig($supervisorScheduleBean, $supervisorScheduleBean->getGlobalId());
                });
            SupervisorConfig::buildProcessConfigFile();
        } catch (Exception $exception) {
            //如果任务处理失败，记录日志，后续可以加入邮件通知
            dd($exception);
        }
    }

    /**
     * supervisorScheduleFilter
     *
     * @param SupervisorScheduleBean $supervisorScheduleBean
     * @param string                 $supervisorGlobalId
     * @return bool
     * @throws Exception
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:01:10
     */
    private function supervisorScheduleFilter(SupervisorScheduleBean $supervisorScheduleBean, string $supervisorGlobalId): bool
    {
        /**
         * 1：启动时间未到则不启动
         * 2：如果结束时间设置，且截止时间到了则不启动
         * 3：如果对应supervisor节点中的任务已经完成则不启动
         */

        if ($supervisorScheduleBean->getTimeBegin() > time()) {
            return true;
        }
        if ($supervisorScheduleBean->getTimeEnd() && $supervisorScheduleBean->getTimeEnd() < time()) {
            return true;
        }
        $supervisorProcesses = $supervisorScheduleBean->getSupervisorProcess($supervisorGlobalId);
        if (empty($supervisorProcesses)) {
            return true;
        }
        if (count($supervisorProcesses) > $supervisorScheduleBean->getProcessNum()) {
            return true;
        }

        switch ($supervisorScheduleBean->getType()) {
            case 1:
                //过滤掉执行完成的一次性任务
                foreach ($supervisorProcesses as $supervisorProcess) {
                    if ($supervisorProcess->getRunStatus() != SupervisorProcessStateEnum::valueForKey(
                            SupervisorProcessStateEnum::SUPERVISOR_PROCESS_STATE_EXITED)) {
                        return false;
                    }
                }
                break;
            case 2:
                //定时任务还需要检测 是否满足定时任务执行的条件
                if (CronExpression::isValidExpression($supervisorScheduleBean->getCron())) {
                    return true;
                }
                return !CronExpression::factory($supervisorScheduleBean->getCron())
                                      ->isDue();
                break;
            case 3:
            default:
        }

        return false;
    }

    /**
     * makeSupervisorProgramConfig
     *
     * @param SupervisorScheduleBean $supervisorScheduleBean
     * @param string                 $supervisorGlobalId
     *
     * @author zhanglei <1070968166@qq.com>
     * @date   2020-04-04 15:01:18
     */
    private function makeSupervisorProgramConfig(SupervisorScheduleBean $supervisorScheduleBean, string $supervisorGlobalId)
    {
        foreach ($supervisorScheduleBean->getSupervisorProcess($supervisorGlobalId) as $supervisorProcessBean) {
            if ($supervisorProcessBean->getStatus() != 1) continue;
            $supervisorProgramConfig = new SupervisorProgramConfig();
            $supervisorProgramConfig->setProgram($supervisorProcessBean->getName());
            $supervisorProgramConfig->setProcessName($supervisorProcessBean->getName());
            $supervisorProgramConfig->setDirectory($supervisorScheduleBean->getDirectory());
            $supervisorProgramConfig->setCommand(" {$supervisorScheduleBean->getLanguage()} {$supervisorScheduleBean->getShell()} {$supervisorProcessBean->getShell()}");
            $supervisorProgramConfig->setNumprocs(1);
            $supervisorProgramConfig->setStartretries($supervisorScheduleBean->getStartRetries() ?? 3);
            $supervisorProgramConfig->setAutostart($supervisorScheduleBean->getAutoStart() ? "true" : "false");
            $supervisorProgramConfig->setAutorestart($supervisorScheduleBean->getAutoRestart() ? "true" : "false");
            switch ($supervisorScheduleBean->getType()) {
                case 1:
                    //过滤掉执行完成的一次性进程
                    if ($supervisorProcessBean->getRunStatus() == SupervisorProcessStateEnum::valueForKey(
                            SupervisorProcessStateEnum::SUPERVISOR_PROCESS_STATE_EXITED)) {
                        continue 2;
                    }
                    break;
                case 2:
                    break;
                case 3:
                    $supervisorProgramConfig->setAutostart("true");
                    $supervisorProgramConfig->setAutorestart("true");
                    break;
                default:
                    continue 2;
            }
            SupervisorConfig::$processConfigList[$supervisorProcessBean->getName()] = $supervisorProgramConfig;
        }
    }

}
