<?php

namespace App\Providers;

use App\Events\SupervisorEvent\SupervisorProcessState;
use App\Events\SupervisorEvent\SupervisorStateChange;
use App\Events\SupervisorEvent\SupervisorTick;
use App\Listeners\SupervisorEventListener\SupervisorProcessStateListener;
use App\Listeners\SupervisorEventListener\SupervisorStateChangeListener;
use App\Listeners\SupervisorEventListener\SupervisorTickListener;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen
        = [
            SupervisorProcessState::class => [
                SupervisorProcessStateListener::class
            ],
            SupervisorStateChange::class  => [
                SupervisorStateChangeListener::class
            ],
            SupervisorTick::class         => [
                SupervisorTickListener::class
            ],
        ];
}
